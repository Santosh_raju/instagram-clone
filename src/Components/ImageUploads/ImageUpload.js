import { Button } from "@material-ui/core";
import React, { useState } from "react";
import { db, storage } from "../../Firebase";
import firebase from "firebase";
import "./ImageUpload.css";

function ImageUpload({ username }) {
  const [caption, setCaption] = useState("");
  const [progress, setProgress] = useState(0);
  const [image, setImage] = useState(null);

  const handleChange = (e) => {
    if (e.target.files[0]) {
      setImage(e.target.files[0]);
    }
  };

  const handleUpload = () => {
    const uploadTask = storage.ref(`images/${image.name}`).put(image);

    uploadTask.on(
      "state_changed",
      (snapshot) => {
        //Progress Function..
        const progress = Math.round(
          (snapshot.bytesTransferred / snapshot.totalBytes) * 100
        );
        setProgress(progress);
      },
      (error) => {
        //Error Function..
        console.log(error);
        alert(error.message);
      },
      () => {
        //Complete Function..
        storage
          .ref("images")
          .child(image.name)
          .getDownloadURL()
          .then((url) => {
            //POst image inside DB
            db.collection("posts").add({
              timestamp: firebase.firestore.FieldValue.serverTimestamp(),
              caption: caption,
              imageUrl: url,
              username: username,
            });

            setProgress(0);
            setCaption("");
            setImage(null);
          });
      }
    );
  };

  return (
    <div className="image_upload">
      <progress className="imageupload_progress" value={progress} max="100" />
      <input
        style={{ marginBottom: "10px" }}
        type="text"
        placeholder="Enter a Caption.."
        onChange={(e) => setCaption(e.target.value)}
        value={caption}
      />
      <input
        type="file"
        onChange={handleChange}
        style={{ marginBottom: "10px" }}
      />
      <Button onClick={handleUpload} variant="contained" color="secondary">
        Upload
      </Button>
    </div>
  );
}

export default ImageUpload;
