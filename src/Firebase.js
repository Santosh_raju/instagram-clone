import firebase from "firebase";

// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyDQFs2L8jJ3rEHimWZRg0y3jk_0j6FOjnQ",
  authDomain: "instagram-clone-96c28.firebaseapp.com",
  databaseURL: "https://instagram-clone-96c28.firebaseio.com",
  projectId: "instagram-clone-96c28",
  storageBucket: "instagram-clone-96c28.appspot.com",
  messagingSenderId: "335896263301",
  appId: "1:335896263301:web:1bde4d3be3066c048047ec",
  measurementId: "G-GYXY8CYRCS",
};

const firebaseApp = firebase.initializeApp(firebaseConfig);

const db = firebaseApp.firestore();
const auth = firebase.auth();
const storage = firebase.storage();

export { db, auth, storage };
